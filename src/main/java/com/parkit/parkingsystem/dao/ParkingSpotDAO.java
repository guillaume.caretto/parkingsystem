package com.parkit.parkingsystem.dao;

import com.parkit.parkingsystem.config.DataBaseConfig;
import com.parkit.parkingsystem.constants.DBConstants;
import com.parkit.parkingsystem.constants.ParkingType;
import com.parkit.parkingsystem.model.ParkingSpot;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ParkingSpotDAO {
    private static final Logger logger = LogManager.getLogger("ParkingSpotDAO");

    public DataBaseConfig dataBaseConfig = new DataBaseConfig();
    //TODO SINGLETON

    public int getNextAvailableSlot(ParkingType parkingType) {
        int result = -1;
        try (PreparedStatement ps = dataBaseConfig.getConnection().prepareStatement(DBConstants.GET_NEXT_PARKING_SPOT)) {
            ps.setString(1, parkingType.toString());
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    result = rs.getInt(1);
                }
            }
        } catch (Exception ex) {
            logger.error("Error fetching next available slot", ex);
        }
        return result;
    }

    public void updateParking(ParkingSpot parkingSpot) {
        //update the availability fo that parking slot
        try (PreparedStatement ps = dataBaseConfig.getConnection().prepareStatement(DBConstants.UPDATE_PARKING_SPOT)) {
            ps.setBoolean(1, parkingSpot.isAvailable());
            ps.setInt(2, parkingSpot.getId());
            ps.executeUpdate();
        } catch (Exception ex) {
            logger.error("Error updating parking info", ex);
        }
    }

}
