package com.parkit.parkingsystem.constants;

public class LogConstants
{
        private LogConstants() {
                throw new UnsupportedOperationException("This is a constant class and cannot be instantiated");
        }
        public static final String FETCH_ERR = "Error fetching next available slot";

        public static final String SAVE_ERR = "Error saving ticket info";

}
