package com.parkit.parkingsystem.constants;

public class FareUtils {
    private FareUtils() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }
    public static final double BIKE_RATE_PER_HOUR = 1.0;
    public static final double CAR_RATE_PER_HOUR = 1.5;

    public static final double DISCOUNT = 0.95;
}
