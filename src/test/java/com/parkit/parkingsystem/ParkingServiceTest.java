package com.parkit.parkingsystem;

import com.parkit.parkingsystem.constants.ParkingType;
import com.parkit.parkingsystem.dao.ParkingSpotDAO;
import com.parkit.parkingsystem.dao.TicketDAO;
import com.parkit.parkingsystem.model.ParkingSpot;
import com.parkit.parkingsystem.model.Ticket;
import com.parkit.parkingsystem.service.ParkingService;
import com.parkit.parkingsystem.util.InputReaderUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class ParkingServiceTest {

    private static final Logger logger = LogManager.getLogger("ParkingServiceTest");
    private static ParkingService parkingService;
    @Mock
    private static InputReaderUtil inputReaderUtil;
    @Mock
    private static ParkingSpotDAO parkingSpotDAO;
    @Mock
    private static TicketDAO ticketDAO;

    @BeforeEach
    public void setUpPerTest() {
        try {
            parkingService = new ParkingService(inputReaderUtil, parkingSpotDAO, ticketDAO);
        } catch (Exception e) {
            logger.error(e);
            throw new RuntimeException("Failed to set up test mock objects");
        }
    }

    @Test
    void processExitingVehicleTest() {
        // Given
        ParkingSpot parkingSpot = new ParkingSpot(1, ParkingType.CAR, false);
        Ticket ticket = new Ticket();
        ticket.setInTime(new Date(System.currentTimeMillis() - (60 * 60 * 1000)));
        ticket.setParkingSpot(parkingSpot);
        ticket.setVehicleRegNumber("ABCDEF");
        when(inputReaderUtil.readVehicleRegistrationNumber()).thenReturn("ABCDEF");
        when(ticketDAO.getTicket(anyString())).thenReturn(ticket);
        when(ticketDAO.updateTicket(any(Ticket.class))).thenReturn(true);
        when(ticketDAO.getNbTicket(anyString())).thenReturn(0);
        // When
        parkingService.processExitingVehicle();
        // Then
        verify(parkingSpotDAO, Mockito.times(1)).updateParking(any(ParkingSpot.class));
        verify(ticketDAO, times(1)).getTicket(anyString());
        verify(ticketDAO, times(1)).updateTicket(any(Ticket.class));
        verify(ticketDAO, times(1)).getNbTicket(anyString());
    }

    @Test
    void processIncomingVehicleTest() {
        // Given
        when(inputReaderUtil.readVehicleRegistrationNumber()).thenReturn("ABCDEF");
        when(inputReaderUtil.readSelection()).thenReturn(1);
        when(parkingSpotDAO.getNextAvailableSlot(any(ParkingType.class))).thenReturn(1);
        // When
        parkingService.processIncomingVehicle();
        // Then
        verify(parkingSpotDAO, times(1)).updateParking(any(ParkingSpot.class));
        verify(ticketDAO, times(1)).saveTicket(any(Ticket.class));
        verify(parkingSpotDAO).getNextAvailableSlot(any(ParkingType.class));
        verify(ticketDAO, never()).getTicket("ABCDEF");
    }

    @Test
    void processExitingVehicleCouldNotUpdateTicketTest() {
        // Given
        when(inputReaderUtil.readVehicleRegistrationNumber()).thenReturn("ABCDEF");
        ParkingSpot parkingSpot = new ParkingSpot(1, ParkingType.CAR, false);
        Ticket ticket = new Ticket();
        ticket.setInTime(new Date(System.currentTimeMillis() - (60 * 60 * 1000)));
        ticket.setParkingSpot(parkingSpot);
        ticket.setVehicleRegNumber("ABCDEF");
        when(ticketDAO.getTicket(anyString())).thenReturn(ticket);
        when(ticketDAO.updateTicket(ticket)).thenReturn(false);
        // When
        parkingService.processExitingVehicle();
        // Then
        verify(parkingSpotDAO, Mockito.times(0)).updateParking(any(ParkingSpot.class));
        verify(ticketDAO, times(1)).getTicket(anyString());
        verify(ticketDAO, times(1)).updateTicket(any(Ticket.class));
    }

    @Test
    void getNextParkingNumberIfAvailableTest() {
        // Given
        ParkingType parkingType = ParkingType.CAR;
        when(inputReaderUtil.readSelection()).thenReturn(1);
        when(parkingSpotDAO.getNextAvailableSlot(parkingType)).thenReturn(1);
        // When
        ParkingSpot parkingSpot = parkingService
                .getNextParkingNumberIfAvailable();
        // Then
        verify(parkingSpotDAO, times(1))
                .getNextAvailableSlot(any(ParkingType.class));
        verify(inputReaderUtil, times(1)).readSelection();
        assertEquals(ParkingType.CAR, parkingSpot.getParkingType());
    }

    @Test
    void testGetNextParkingNumberIfAvailableParkingNumberNotFound() {
        // Given
        when(inputReaderUtil.readSelection()).thenReturn(2);

        when(parkingSpotDAO.getNextAvailableSlot(any(ParkingType.class)))
                .thenReturn(0);
        // When
        ParkingSpot parkingSpot = parkingService
                .getNextParkingNumberIfAvailable();
        // Then
        verify(parkingSpotDAO, times(1))
                .getNextAvailableSlot(any(ParkingType.class));
        verify(inputReaderUtil, times(1)).readSelection();
        assertNull(parkingSpot);
    }

    @Test
    void testGetNextParkingNumberIfAvailableParkingNumberWrongArgument() {
        // Given
        when(inputReaderUtil.readSelection()).thenReturn(3);
        // When
        ParkingSpot parkingSpot = parkingService
                .getNextParkingNumberIfAvailable();
        // Then
        verify(parkingSpotDAO, times(0))
                .getNextAvailableSlot(any(ParkingType.class));
        verify(inputReaderUtil, times(1)).readSelection();
        assertNull(parkingSpot);
    }
}