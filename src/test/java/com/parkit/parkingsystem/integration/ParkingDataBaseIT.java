package com.parkit.parkingsystem.integration;

import com.parkit.parkingsystem.constants.FareUtils;
import com.parkit.parkingsystem.constants.ParkingType;
import com.parkit.parkingsystem.dao.ParkingSpotDAO;
import com.parkit.parkingsystem.dao.TicketDAO;
import com.parkit.parkingsystem.integration.config.DataBaseTestConfig;
import com.parkit.parkingsystem.integration.service.DataBasePrepareService;
import com.parkit.parkingsystem.model.Ticket;
import com.parkit.parkingsystem.service.ParkingService;
import com.parkit.parkingsystem.util.InputReaderUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ParkingDataBaseIT {

    private static final String TICKET_NUMBER = "ABCDEF";
    public static DataBaseTestConfig dataBaseTestConfig = new DataBaseTestConfig();
    private static ParkingSpotDAO parkingSpotDAO;
    private static TicketDAO ticketDAO;
    private static DataBasePrepareService dataBasePrepareService;

    @Mock
    private static InputReaderUtil inputReaderUtil;

    @BeforeAll
    public static void setUp() {
        parkingSpotDAO = new ParkingSpotDAO();
        parkingSpotDAO.dataBaseConfig = dataBaseTestConfig;
        ticketDAO = new TicketDAO();
        ticketDAO.dataBaseConfig = dataBaseTestConfig;
        dataBasePrepareService = new DataBasePrepareService();
    }

    @AfterAll
    public static void tearDown() {

    }

    @BeforeEach
    public void setUpPerTest() throws Exception {
        when(inputReaderUtil.readSelection()).thenReturn(1);
        when(inputReaderUtil.readVehicleRegistrationNumber()).thenReturn("ABCDEF");
        dataBasePrepareService.clearDataBaseEntries();
    }

    @Test
    void testParkingACar() {
        int previousSlot = parkingSpotDAO.getNextAvailableSlot(ParkingType.CAR);

        ParkingService parkingService = new ParkingService(inputReaderUtil, parkingSpotDAO, ticketDAO);
        parkingService.processIncomingVehicle();

        // check that a ticket is actualy saved in DB
        Ticket ticket = ticketDAO.getTicket(TICKET_NUMBER);
        assertNotNull(ticket);
        assertFalse(ticket.getParkingSpot().isAvailable());

        // check that Parking table is updated with availability
        int newSlot = parkingSpotDAO.getNextAvailableSlot(ParkingType.CAR);
        assertNotEquals(previousSlot, newSlot);
    }

    @Test
    void testParkingLotExit() {
        testParkingLotExit(false);
    }

    public void testParkingLotExit(boolean recurring) {
        testParkingACar();
        Ticket ticket = ticketDAO.getTicket(TICKET_NUMBER);
        ParkingService parkingService = new ParkingService(inputReaderUtil, parkingSpotDAO, ticketDAO);
        long outTime = ticket.getInTime().getTime() + 60 * 60 * 1000;
        parkingService.processExitingVehicle(new Date(outTime));

        // check that the fare generated and out time are populated correctly in the database
        ticket = ticketDAO.getTicket(TICKET_NUMBER);
        assertNotNull(ticket);
        assertEquals(outTime, ticket.getOutTime().getTime());
        assertEquals(FareUtils.CAR_RATE_PER_HOUR * (recurring ? FareUtils.DISCOUNT : 1), ticket.getPrice());
    }

    @Test
    void testParkingLotExitRecurringUser() {
        testParkingACar();
        testParkingLotExit(true);
    }
}
