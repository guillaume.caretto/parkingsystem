package com.parkit.parkingsystem;

import com.parkit.parkingsystem.constants.ParkingType;
import com.parkit.parkingsystem.model.ParkingSpot;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static junit.framework.Assert.*;

class ParkingSpotTest {
    ParkingSpot parkingSpot;

    @BeforeEach
    void setUpTest() {
        parkingSpot = new ParkingSpot(1, ParkingType.CAR, true);
    }

    @Test
    void equalsTest_withAnEqualObject_shouldReturnTrue() {
        Object o = new ParkingSpot(1, ParkingType.CAR, true);
        boolean result = parkingSpot.equals(o);
        assertTrue(result);
    }

    @Test
    void equalsTest_sammeParkingSpotObjcet_shouldReturnTrue() {

        boolean result = parkingSpot.equals(parkingSpot);
        assertTrue(result);
    }

    @Test
    void equalsTest_withAnUnEqualObject_shouldReturnFalse() {

        Object o = new ParkingSpot(4, ParkingType.BIKE, true);
        boolean result = parkingSpot.equals(o);
        assertFalse(result);
    }

    @Test
    void equalsTest_withNewObject_shouldReturnFalse() {
        Object o = new Object();
        boolean result = parkingSpot.equals(o);
        assertFalse(result);
    }

    @Test
    void equalsTest_withNullObject_shouldReturnFalse() {
        boolean result = parkingSpot.equals(null);
        assertFalse(result);
    }

    @Test
    void HashCodeTest() {
        int result = parkingSpot.hashCode();
        assertEquals(1, result);
    }
}